Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: dpf-plugins
Upstream-Contact: Filipe Coelho <falktx@falktx.com>
Source: https://github.com/DISTROHO/DPF-Plugins

Files: *
Copyright: 2012-2019, Filipe Coeho <falktx@falktx.com>
License: GPL-2+

Files: debian/*
Copyright:
  2012-2018, Filipe Coelho <falktx@falktx.com>
  2019, Erich Eickmeyer <erich@ericheickmeyer.com>
  2020-2021 Dennis Braun <d_braun@kabelmail.de>
License: GPL-2+

Files: debian/watch
Copyright:
  2019, Thomas Ward <teward@ubuntu.com>
License: GPL-2+

Files: dpf/dgl/src/pugl/*
Copyright:
  2012-2014, David Robillard <http://drobilla.net>
  2012-2019, Filipe Coelho <falktx@falktx.com>
License: ISC

Files: dpf/dgl/src/pugl/pugl_x11.c
Copyright:
  2012-2014, David Robillard <http://drobilla.net>
  2011-2012, Ben Loftis, Harrison Consoles
  2013,2015, Robin Gaereus <robin@gareus.org>
  2012-2019, Filipe Coelho <falktx@falktx.com>
License: ISC 

Files: dpf/dgl/src/resources/*
Copyright: Copyright (c) 2003 by Bitstream, Inc. All Rights Reserved. 
 Bitstream Vera is a trademark of Bitstream, Inc.
 DejaVu changes are in public domain.
License: bitstream-vera
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of the fonts accompanying this license ("Fonts") and associated
 documentation files (the "Font Software"), to reproduce and distribute the
 Font Software, including without limitation the rights to use, copy, merge,
 publish, distribute, and/or sell copies of the Font Software, and to permit
 persons to whom the Font Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright and trademark notices and this permission notice shall
 be included in all copies of one or more of the Font Software typefaces.
 .
 The Font Software may be modified, altered, or added to, and in particular
 the designs of glyphs or characters in the Fonts may be modified and
 additional glyphs or characters may be added to the Fonts, only if the fonts
 are renamed to names not containing either the words "Bitstream" or the word
 "Vera".
 .
 This License becomes null and void to the extent applicable to Fonts or Font
 Software that has been modified and is distributed under the "Bitstream
 Vera" names.
 .
 The Font Software may be sold as part of a larger software package but no
 copy of one or more of the Font Software typefaces may be sold by itself.
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF COPYRIGHT, PATENT,
 TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL BITSTREAM OR THE GNOME
 FOUNDATION BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, INCLUDING
 ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER DEALINGS IN THE
 FONT SOFTWARE.
 .
 Except as contained in this notice, the names of Gnome, the Gnome
 Foundation, and Bitstream Inc., shall not be used in advertising or
 otherwise to promote the sale, use or other dealings in this Font Software
 without prior written authorization from the Gnome Foundation or Bitstream
 Inc., respectively. For further information, contact: fonts at gnome dot
 org.
 
Files: dpf/dgl/src/nanovg/*
Copyright: 2008-2010, Bjoern Hoehrmann <bjoern@hoehrmann.de>
           2009-2013, Mikko Mononen <memon@inside.org>
License: ZLIB

Files: dpf/dgl/src/sofd/libsofd*
Copyright: 2014, Robin Gareus <robin@gareus.org>
License: Expat

Files: dpf/distrho/extra/*
Copyright:
  2012-2016 Filipe Coelho <falktx@falktx.com>
  2004-2008 René Nyffenegger
License: ISC

Files: dpf/distrho/src/dssi/*
Copyright: 
  2004-2009, Chris Cannam
  2004-2009, Steve Harris
  2004-2009, Sean Bolton
License: LGPL-2.1+

Files: dpf/distrho/src/ladspa/*
Copyright:
  2000-2002, Richard W.E. Furse
  2000-2002, Paul Barton-Davis
  2000-2002, Stefan Westerfeld
License: LGPL-2.1+

Files: dpf/distrho/src/lv2/*
Copyright:
  2000-2002, Richard W.E. Furse
  2000-2002, Paul Barton-Davis
  2000-2002, Stefan Westerfeld
  2006-2012, Steve Harris
  2006-2012, David Robillard
License: ISC

Files: dpf/distrho/src/lv2/lv2_kxstudio_properties.h
Copyright: 2014, Filipe Coelho <falktx@falktx.com>
License: ISC

Files: dpf/distrho/src/lv2/lv2-midifunctions.h
       dpf/distrho/src/lv2/lv2-miditype.h
Copyright: 2006, Lars Luthman <lars.luthman@gmail.com>
License: LGPL-2+

Files: dpf/distrho/src/vestige/*
Copyright: 2006, Javier Serrano Polo <jasp00@users.sourceforge.net>
License: GPL-2+

Files: plugins/common/*
Copyright: 2015, Filipe Coelho <falktx@falktx.com>
License: ISC

Files: plugins/common/gen_dsp/*
Copyright: 2012, Cycling '74
License: Expat

Files: plugins/glBars/*
Copyright:
  1998-2000, Peter Alm, Mikael Alm, Olle Hallnas, Thomas Nilsson and 4Front Technologies
  2000, Christian Zander <phoenix@minion.de>
  2015, Nedko Arnaudov 
  2016-2019, Filipe Coelho <falktx@falktx.com>
License: GPL-3+

Files: plugins/Kars/*
Copyright: 2012-2019, Filipe Coelho <falktx@falktx.com>
License: ISC

Files: plugins/bitcrush/*
       plugins/freeverb/*
       plugins/gigaverb/*
       plugins/pitchshift/*
Copyright: 2015, Filipe Coelho <falktx@falktx.com>
License: LGPL-3+

Files: plugins/3BandEQ/*
       plugins/3BandSplitter/*
       plugins/PingPongPan/*
Copyright:
 2012-2015, Filipe Coelho <falktx@falktx.com
 2007, Michael Gruhn <michael-gruhn@web.de>
License: LGPL-3+

Files: plugins/AmplitudeImposer*
       plugins/CycleShifter/*
       plugins/SoulForce/*
Copyright:
  2004-2006, Niall Moody
  2015, Filipe Coelho <falktx@falktx.com>
License: Expat

Files: plugins/MVerb/*
Copyright:
  2010, Martin Eastwood
  2015, Filipe Coelho <falktxx@falktx.com>
License: GPL-3+

Files: plugins/Nekobi/*
Copyright:
  2004, Sean Bolton and others
  2013-2015, Filipe Coelho <falktx@falktx.com>
License: GPL-2+

Files: plugins/ProM/*
Copyright:
  2015, Filipe Coelho <falktx@falktx.com
License: LGPL-3+

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any purpose with
 or without fee is hereby granted, provided that the above copyright notice and this
 permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
 TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN
 NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian GNU systems, the complete text of the GNU General Public
 License (GPL) version 2 or later can be found in
 '/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
Comment: On Debian systems the complete text of the GNU General Public
 License version 3 can be found at `/usr/share/common-licenses/GPL-3'.
 
License: LGPL-2+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2'.
 .
 You should have received a copy of the GNU Library General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 General Public License for more details.
 .
 On Debian GNU systems, the complete text of the GNU Lesser General
 Public License (LGPL) version 2.1 or later can be found in
 '/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program.  If not, see
 <https://www.gnu.org/licenses/>.

License: LGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/LGPL-3'.

License: ZLIB
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
